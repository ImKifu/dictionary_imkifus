package imkifu.dictionary;

import java.io.IOException;

public class Main {
    public static void main( String argv[] ) throws IOException
    {


        DictionaryCommandline n = new DictionaryCommandline();
        //n.insertFromCommandline();
        n.insertFromFile();
        //n.showAllWords();
        n.dictionaryLookup();
    }
}

