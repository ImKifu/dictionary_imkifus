package imkifu.dictionary;

public class Word {
    private String word_taget;
    private String word_explain;

    public void setWord_taget(String s)
    {
        this.word_taget = s;
    }
    public String getWord_taget()
    {
        return word_taget;
    }
    public void setWord_explain(String s)
    {
        this.word_explain = s;
    }
    public String getWord_explain()
    {
        return word_explain;
    }
}
